/* utils.js */
/**
* Returns whether provided value is a function
* @param {*} value - the value to be checked
* @return {boolean} true if the value is a function, false otherwise
*/

export function isFunction(value) {
    return typeof value === 'function';
}
