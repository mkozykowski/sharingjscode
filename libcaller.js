/*
The utils module exports an isFunction() function that returns whether the provided parameter is a function.
Here’s a c-libcaller Lightning web component that uses the utils module.
*/

/* libcaller.js */
import { LightningElement } from 'lwc';
// import the library
import { isFunction } from 'c/utils';

export default class LibCaller extends LightningElement {
    result;
    checkType() {
        // Call the imported library function
        this.result = isFunction(
            function() {
                console.log('I am a function');
            }
        );
    }
}
